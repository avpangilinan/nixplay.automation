package framework.keyword;

import framework.pages.FramePage;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;


/**
 * @author alexander.v.pangilinan
 */
public class FrameKeyword extends FramePage {

    public FrameKeyword(WebDriver driver, Logger log) {

        super(driver, log);
    }

    public void validate_product_name_displayed(String product_name) {
        waitUntilPageready();
        waitForElementToBeVisible(product_header(product_name),6000);
        isDiplayed(product_header(product_name));
    }
    public void validate_overview_tab_displayed() {
        waitForElementToBeVisible(overview_tab,6000);
        isDiplayed(overview_tab);
    }
    public void validate_tech_specs_displayed() {
        waitForElementToBeVisible(tech_specs_tab,6000);
        isDiplayed(tech_specs_tab);
    }
}
