package framework.keyword;

import framework.commonFunctions.ConfigurationKeyEnum;
import framework.pages.HomePage;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

/**
 * @author alexander.v.pangilinan
 */
public class HomeKeyword extends HomePage {

    public HomeKeyword(WebDriver driver, Logger log) {

        super(driver, log);
    }

    public void openPage(String url) {
        log.info("Open " + url + " Environment.");
        getPage(url);
        waitUntilPageready();
    }

    public void closeNewsLetterPanel() {
        log.info("Close NewsLetter Panel.");
        try {
            for (int i = 0; i < 3; i++) {
                waitForElementToBeVisible(newsLetter_label, 10000);
                for (int x = 0; x < 3; x++) {
                    if (checkElementIfInvisible(newsLetter_label, 1000) == true) {
                        break;
                    }
                    sleep(Duration.ofSeconds(1));
                    click(close_button);
                    waitForElementToBeInvisible(newsLetter_label, 1000);
                }
                if (elementCount(newsLetter_label) == 0) {
                    log.info("The newsletter panel is now hidden.");
                    break;
                }
            }
        }catch (Exception e){
            log.info("The The newsletter panel did not appear.");
        }

    }

    public void selectTab(String nixplay_tab){
        log.info("Open NixPlay tab "+nixplay_tab);
        waitUntilPageready();
        waitForElementToBeVisible(nixplay_frames_button(nixplay_tab), 5000);
        click(nixplay_frames_button(nixplay_tab));
        waitUntilPageready();
    }



}
