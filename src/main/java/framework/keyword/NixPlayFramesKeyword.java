package framework.keyword;

import framework.pages.HomePage;
import framework.pages.NixPlayFramesPage;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import javax.validation.constraints.AssertTrue;
import java.time.Duration;

/**
 * @author alexander.v.pangilinan
 */
public class NixPlayFramesKeyword extends NixPlayFramesPage {

    public NixPlayFramesKeyword(WebDriver driver, Logger log) {

        super(driver, log);
    }

    public void clickFrame(String frame_size) {
        waitUntilPageready();
        waitForElementToBeVisible(frame_button(frame_size),3000);
        click(frame_button(frame_size));
    }



}
