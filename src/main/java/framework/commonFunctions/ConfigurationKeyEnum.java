package framework.commonFunctions;

/**
 * Configuration keys enum in properties.
 * @author alexander.v.pangilinan
 */
public enum ConfigurationKeyEnum {

    url("url"),
    password("password"),
    username("username"),
    email("email"),
    mobile("mobile");

    private final String value;

    ConfigurationKeyEnum(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static ConfigurationKeyEnum getByValue(String value){
        for(ConfigurationKeyEnum currentValue: values()){
            if(currentValue.value().equals(value)){
                return currentValue;
            }
        }
        throw new RuntimeException("Failed to find ConfigurationKeyEnum");
    }
}
