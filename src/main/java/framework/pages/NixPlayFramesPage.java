package framework.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author alexander.v.pangilinan
 * */
public class NixPlayFramesPage extends BasePageObject<NixPlayFramesPage> {

    protected NixPlayFramesPage(WebDriver driver, Logger log) {
        super(driver, log);
    }
    protected By frame_button(String frame_size) {
       return By.xpath("//span[normalize-space()='"+frame_size+"\" inch']//preceding-sibling::a");
    }

}
