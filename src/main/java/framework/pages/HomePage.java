package framework.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author alexander.v.pangilinan
 * */
public class HomePage extends BasePageObject<HomePage> {

    protected HomePage(WebDriver driver, Logger log) {
        super(driver, log);
    }

    protected By close_button = By.xpath("//button[contains(@class,'close-form')]");
    protected By nixplay_frames_button(String tab_name){
     return By.xpath("//nav//a[normalize-space()='"+tab_name+"' and @class]");
    }
    protected By newsLetter_label = By.xpath("//strong[contains(text(),'JOIN OUR NEWSLETTER TODAY')]");
}
