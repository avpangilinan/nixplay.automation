package framework.pages;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author alexander.v.pangilinan
 * */
public class FramePage extends BasePageObject<FramePage> {

    protected FramePage(WebDriver driver, Logger log) {
        super(driver, log);
    }
    protected By overview_tab = By.xpath("//a[normalize-space()='Overview' and contains(@class,'link overview-link')]");
    protected By tech_specs_tab = By.xpath("//a[normalize-space()='Tech Specs' and contains(@class,'link techspecs-link')]");
    public By product_header(String product_name){
        return By.xpath("//div[@class='toggle-submenu']//h1[contains(text(),'"+product_name+"')]");
    }
}
