package framework.environment;

import framework.commonFunctions.CsvDataProvider;


import io.cucumber.java.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

/**
 * Environment initialization.
 * Please do not modify or if needed kindly communicate with the author
 * @author alexander.v.pangilinan
 * */

public class BaseTestCase<V>{
	public static WebDriver driver;
	public WebDriverWait wait;
	public String testname;
	public String dateTime;
	public String browser;
	public static Logger log;
	public ITestContext iTestContext;

	static {
		try {
			configuration();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void configuration(){
		Properties globalConfig = loadProp("config properties/config.properties");
		System.getProperties().putAll(globalConfig);
	}

	@SuppressWarnings("static-access")
	public void InitializeTestEnvironment() {
		BrowserFactory BrowserFactory = new BrowserFactory();
		this.dateTime = DateTimeFormatter.ofPattern("ddMMYYhhmm").format(ZonedDateTime.now());
		this.testname = "Cucumber";
		try {
			if (browser == null || browser.length() == 0) {
				String Scriptbrowser = "chrome";
				this.log = LogManager.getLogger(testname + " in " + Scriptbrowser);

			} else {
				this.log = LogManager.getLogger(testname +" "+ browser);

			}
		} catch (Exception e) {

		}
		for (int y = 1; y <= 5; y++) {
			String reportDirectory = new File(System.getProperty("user.dir")).getAbsolutePath()
					+ "/target/surefire-reports" + getPastDateString(y);
			Path directory = Paths.get(reportDirectory);
			try {
				deleteFolderAndItsContent(directory);
			} catch (IOException e2) {
			}
		}

		try {
				if (browser == null || browser.length() == 0) {
					log.info("The test is running in local environment and triggered via script.");
					String DbrowserName = "chrome";
					this.driver = BrowserFactory.getDriver(DbrowserName, "No");
					this.browser = DbrowserName;
				} else {
					log.info("The test is running in local environment and triggered via test suite.");
					this.driver = BrowserFactory.getDriver(browser, "No");
					this.browser = browser;
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}


	public Date yesterday(int x) {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -x);
		return cal.getTime();
	}

	public String getPastDateString(int x) {
		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
		return dateFormat.format(yesterday(x));
	}

	public static WebDriver getDriver() {
		return driver;
	}
	public static Logger getlog() {
		return log;
	}
	public WebDriverWait getwait() {
		return this.wait;
	}


	public static void deleteFolderAndItsContent(final Path folder) throws IOException {
		Files.walkFileTree(folder, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				if (exc != null) {
					throw exc;
				}
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}
	private static Properties loadProp(String propFileName) {
		Properties prop = new Properties();
		ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
		URL globalConfigUrl = currentClassLoader.getResource(propFileName);

		try {
			prop.load(globalConfigUrl.openStream());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return prop;
	}

	@DataProvider(name = "DefaultProvider")
	public Iterator<Object[]> getDataProvider(ITestContext iTestContext, Method method) throws IOException {
		this.iTestContext = iTestContext;
		return new CsvDataProvider(method);
	}

	public void TearDown(Scenario scenario) {
		log.info("Run completed.");
		driver.quit();
	}



}