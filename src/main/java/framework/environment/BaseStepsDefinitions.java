package framework.environment;

import framework.keyword.FrameKeyword;
import framework.keyword.HomeKeyword;
import framework.keyword.NixPlayFramesKeyword;

public class BaseStepsDefinitions extends BaseTestCase {

    public HomeKeyword homeKeyword;
    public FrameKeyword frameKeyword;
    public NixPlayFramesKeyword nixPlayFramesKeyword;
    public String scenDesc;

    public void baseStepsDefinitions(){
        InitializeTestEnvironment();
        this.homeKeyword = new HomeKeyword(driver, log);
        this.frameKeyword = new FrameKeyword(driver, log);
        this.nixPlayFramesKeyword = new NixPlayFramesKeyword(driver, log);
    }

}
