#@author Alexander Pangilinan
Feature: View frame details.

  Scenario Outline: User wants to get information about 15-inch Wi-Fi Digital frame.
    Given User opens  <url> website
    When User Clicks on <tab_name> tab
    And User Clicks on <inch> option at the top of the page
    Then The <product_name> page is opened
    And <tab1> and <tab2> tab at the right top corner.
    Examples:
      | url                       | tab_name         | inch | product_name                                                       | tab1       | tab2         |
      | "https://www.nixplay.com" | "Nixplay Frames" | "15" | "Nixplay 15-inch Original Wi-Fi Digital Picture Frame (Landscape)" | "Overview" | "Tech Specs" |


