package Runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


/**
 * @author Alexander Pangilinan
 */

@CucumberOptions(features={"src/test/java/Features"}
					,glue={"StepDefinitions"}
					,plugin = {"html:target/cucumber-reports/nixplay_automation/cucumber-pretty","json:target/json-cucumber-reports/nixplay_automation/nixplayAutomation.json",
		"testng:target/testng-cucumber-reports/nixplay_automation/nixplayAutomation.xml"}
//					,tags = "@AccountManagement"
		)
public class RunTest extends AbstractTestNGCucumberTests {
	@BeforeClass
	public static void before() {
		System.out.println("Before - "+System.currentTimeMillis());
	}

	@AfterClass
	public static void after() {
		System.out.println("After - "+System.currentTimeMillis());
	}
}
