package StepDefinitions;

import framework.environment.BaseStepsDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author Alexander Pangilinan
 * */

public class ViewFrameDetails extends BaseStepsDefinitions {

    @Before
    public void before(Scenario scenario) {
        this.scenDesc = scenario.getName();
    }

    @After
    public void after(Scenario scenario){
        TearDown(scenario);
    }


    @Given("User opens  {string} website")
    public void userOpensWebsite(String url) {
        baseStepsDefinitions();
        homeKeyword.openPage(url);
        homeKeyword.closeNewsLetterPanel();

    }

    @When("User Clicks on {string} tab")
    public void userClicksOnTab(String tab_name) {
        homeKeyword.selectTab(tab_name);
    }

    @And("User Clicks on {string} option at the top of the page")
    public void userClicksOnOptionAtTheTopOfThePage(String inch) {
        nixPlayFramesKeyword.clickFrame(inch);
    }

    @Then("The {string} page is opened")
    public void thePageIsOpened(String product_name) {
        frameKeyword.product_header(product_name);
    }

    @And("{string} and {string} tab at the right top corner.")
    public void overviewAndTechSpecsTabAtTheRightTopCorner(String tab1, String tab2){
        frameKeyword.validate_overview_tab_displayed();
        frameKeyword.validate_tech_specs_displayed();
    }


}
